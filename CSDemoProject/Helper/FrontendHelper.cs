﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;

namespace CSDemoProject.Helper
{

    public static class FrontendHelper
    {
        public class FrontendAssets
        {
            public List<String> Scripts = new List<String>();
            public List<String> Styles = new List<String>();

            public FrontendAssets(JObject manifest)
            {

                foreach (var name in new[] { "app.css" })
                    Styles.Add((String)manifest[name]);

                foreach (var name in new[] { "manifest.js", "vendor.js", "app.js" })
                    Scripts.Add((String)manifest[name]);
            }

        }

        private static FrontendAssets _assetsCache;
        public static FrontendAssets Assets
        {
            get
            {
                return LoadAssets();
            }
        }

        public static FrontendAssets LoadAssets()
        {
            // Get cached result
            if (_assetsCache != null)
                return _assetsCache;

            try
            {
                var manifestFile = HttpContext.Current.Server.MapPath("~/Scripts/Frontend/manifest.json");
                if (!File.Exists(manifestFile))
                    throw new Exception("Frontend manifest file does not exist.");
                // Save result for later usage
                _assetsCache = new FrontendAssets(JObject.Parse(File.ReadAllText(manifestFile)));
                return _assetsCache;
            }
            catch (Exception e)
            {
                throw new Exception("Could not read frontend manifest file.", e);
            }
        }
    }
}