﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using CSDemoProject.Models;

namespace CSDemoProject.DAL
{
    public class DefaultContext : DbContext
    {

        public DefaultContext() : base("CSDemoProject")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserGroup> UserGroupAssignments { get; set; }
        public DbSet<Group> Groups { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // We do not want our table names to be pluralized
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}