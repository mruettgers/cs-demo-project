﻿using System.Collections.Generic;

namespace CSDemoProject.Models
{
    public class Group
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<UserGroup> UserGroupAssignments { get; set; }
    }
}