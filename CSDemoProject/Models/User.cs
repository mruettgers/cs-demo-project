﻿using System;
using System.Collections.Generic;

namespace CSDemoProject.Models
{
    public class User
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime RegistrationDate { get; set; }

        public virtual ICollection<UserGroup> UserGroupAssignments { get; set; }
    }
}