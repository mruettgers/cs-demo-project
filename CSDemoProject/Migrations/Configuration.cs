namespace CSDemoProject.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Threading.Tasks;
    using Seeders;
    using DAL;
    
    internal sealed class Configuration : DbMigrationsConfiguration<DefaultContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected async override void Seed(DefaultContext context)
        {
            // Seed some test data after running the migrations
            await (new UserAndGroupSeeder(context)).Seed();
        }
    }
}
