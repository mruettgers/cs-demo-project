﻿using System.Data.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using CSDemoProject.DAL;
using CSDemoProject.Models;
using Bogus;

namespace CSDemoProject.Migrations.Seeders
{
    public class UserAndGroupSeeder
    {

        private readonly DefaultContext context;

        public UserAndGroupSeeder(DefaultContext context)
        {
            this.context = context;
        }

        public async Task Seed()
        {
            // FIXME: When the app is ready for deployment seeding should be restricted to empty tables (!context.Users.Any())
            if (true)
            {
                // Create test groups
                const int numberOfGroupsToSeed = 10;

                int groupNumber = 1;
                var groupFactory = new Faker<Group>()
                    .RuleFor(g => g.Name, (f, g) => String.Format("Group {0:D3}", groupNumber++))
                    .FinishWith((f, g) => context.Groups.Add(g));
                
                var testGroups = groupFactory.Generate(numberOfGroupsToSeed);
                context.SaveChanges();


                // Create test users and assign them to a random number of random groups
                var userFactory = new Faker<User>()
                    .RuleFor(u => u.FirstName, f => f.Name.FirstName())
                    .RuleFor(u => u.LastName, f => f.Name.LastName())
                    .RuleFor(u => u.RegistrationDate, f => f.Date.Past())
                    .FinishWith((f, u) =>
                    {           
                        context.Users.Add(u);
                        var userGroups = new List<Group>();
                        for (int i = 0; i < f.Random.Number(0,3); i++)
                        {
                            var randomGroup = f.Random.Number(0, numberOfGroupsToSeed - 1);
                            // TODO: Refactor the following lines regarding performance and make use of dictionaries.
                            // A lookup using a hash based data structure will reduce our costs to nearly O(1).
                            if (!userGroups.Contains(testGroups[randomGroup]))
                            {
                               userGroups.Add(testGroups[randomGroup]);
                            }
                        }
                        userGroups.ForEach(g =>
                        {
                            // Create the cross reference entities and add them to the UserGroupAssignment table
                            var ug = new UserGroup { User = u, Group = g };
                            context.UserGroupAssignments.Add(ug);
                        });

                    });

                userFactory.Generate(200);

            }
        }

    }
}