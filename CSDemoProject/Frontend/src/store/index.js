import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'
import Vapi from 'vuex-rest-api'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const users = new Vapi({
  baseURL: '//api/',
  state: {
    users: []
  }
})
  .get({
    action: 'listUsers',
    property: 'users',
    path: '/users'
  })
  .getStore()

export default new Vuex.Store({
  modules: {
    users
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})
